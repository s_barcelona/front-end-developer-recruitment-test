Sngular Barcelona Front-end developer Recruitment Test
===========================================

Thank you for taking the time to do our technical test. It consists of three parts:

* [A small project coding test](#project-coding-test)
* [A few technical questions](#technical-questions)
* [A few personal questions](#personal-questions)

We would like you to submit your results in a ZIP file. Please make this a **single** zip file named `{yourname}-frontend-recruiting-test.zip` containing:

1. A single markdown file with the answers to the questions.
2. A folder containing the project coding test.

## Coding Test

### Task requirements

Feel free to spend all the time you want on the exercise as long as the following requirements are met.  

- Please complete the user story below.
- Your code should compile (if necessary) and run in one step. It is ok if there is a build process.
- JavaScript should be used as a platform.
- Feel free to use whatever frameworks / libraries / packages you like.
- Styles can be added with frameworks, libraries, plain css, preprocessors or postprocessors, be imaginative!
- You **must** include tests.
- You must use this placeholder URL for getting the images: `https://jsonplaceholder.typicode.com/photos`

### User Story

- As a **user visiting the root URL**
- I can **see a grid of images that fill the screen on load**
- When I **Scroll the browser, I see more images loaded**
- When I click an image **It is removed from the list and disappears**

#### Acceptance criteria

- The grid should be responsive, showing 3 columns on small devices and more as the device width growths.
- The images can't pop or flicker, they appear smoothly on the screen.

## Technical questions

Please answer the following questions in a markdown file called `Answers to questions.md`.

1. If you had plenty of time and resources, what would you add to the coding test you implemented?
2. How would you check browser and device compatibility? Have you ever had to do this?
3. How do you test your front-end code? Feel free to comment on your preferred approaches and frameworks.
4. Do you know these frameworks? In which cases are they more convenient for?
    - Angular.
    - React.
    - Vue.
    - Svelte.
    - jQuery.

## Personal questions

Please answer the following questions and add them to the markdown file called `Answers to questions.md`.

1. Please describe yourself and your technical skills using JSON.
2. What do you think about communication in the professional environment?
3. What do you think about responsibility in the professional environment?
4. How do you describe an excellent working day?

#### Thanks for your time, we look forward to hear from you!
- The [Sngular Barcelona Tech team](https://sngular.com)
